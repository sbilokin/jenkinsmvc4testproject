﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlankMVC4.Test
{
    [TestFixture]
    public class TestClass
    {
        [Test]
        public void SuccessfulTestMethod()
        {
            Assert.NotNull(this);
        }

        [TestCase(3)]
        public void NotSuccessfulTestMethod(int digit)
        {
            var zero = 0;
            try
            {
                var interestingResult = digit / zero;
            }
            catch (DivideByZeroException)
            {
                Assert.Pass();
            }
        }



        //[TestCase(3)]
        public void NotSuccessfulTestMethod2(int digit)
        {
            var zero = 0;
            var interestingResult = digit / zero;
        }
    }
}
